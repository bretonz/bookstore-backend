package com.eht.training.book_store.ctrl;

import com.eht.training.book_store.service.BookService;
import com.eht.training.book_store.vo.BookVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/book")
public class BookCtrlImpl implements BookCtrl {

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/{isbn}")
    @ResponseBody
    public BookVO getBook(
            @PathVariable("isbn" )int isbn) {
        System.out.println("Contexto creado");
        BookVO bookVO = this.BookService.getBook(isbn);
        return bookVO;
        
    }
    
    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/{isbn}"
    )
    public void deleteBook(
            @PathVariable("isbn") int isbn){
        this.BookService.deleteBook(isbn);
    }
     
    
    @RequestMapping(
            method = RequestMethod.POST,
            value = "/{isbn}/copy")
    public void saveCopyBook(
            @PathVariable("isbn") int isbn) {
        this.BookService.saveCopyBook(isbn);
    }
    
    @Autowired
    private BookService BookService;
}
