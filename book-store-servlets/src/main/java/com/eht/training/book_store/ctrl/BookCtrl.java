package com.eht.training.book_store.ctrl;

import com.eht.training.book_store.vo.BookVO;

/**
 *
 * @author fabian
 */
public interface BookCtrl {
    BookVO getBook(int isbn);
    void deleteBook(int isbn);
    void saveCopyBook(int isbn);
}
