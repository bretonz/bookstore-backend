package com.eht.training.book_store.service;


import com.eht.training.book_store.vo.BookVO;

/**
 *
 * @author fabian
 */
public interface BookService {
    BookVO getBook(int isbn);
    void deleteBook(int isbn);
    void saveCopyBook(int isbn);
}
