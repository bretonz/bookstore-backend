/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.book_store.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class JdbcUtilsImpl implements JdbcUtils{
    @PostConstruct
    public void init(){
        try {
            Class.forName(this.driverClassName).newInstance();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JdbcUtilsImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(JdbcUtilsImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(JdbcUtilsImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public Connection getConnection() throws SQLException {        
        Connection connection =
            DriverManager.getConnection(this.url, this.username, password);        
        return connection;
    }
    
    @Value("${db.url}")
    private String url;
    
    @Value("${db.username}")
    private String username;
    
    @Value("${db.password}")
    private String password;
    
    @Value("${db.driver_class_name}")
    private String driverClassName;
    
}

