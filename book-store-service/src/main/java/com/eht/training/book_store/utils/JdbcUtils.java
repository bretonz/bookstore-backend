/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eht.training.book_store.utils;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author fabian
 */
public interface JdbcUtils {
    Connection getConnection()throws SQLException;
}
