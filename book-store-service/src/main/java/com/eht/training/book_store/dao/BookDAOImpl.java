package com.eht.training.book_store.dao;
import com.eht.training.book_store.entity.Book;
import com.eht.training.book_store.entity.utils.HibernateUtil;
import com.eht.training.book_store.utils.JdbcUtils;
import com.eht.training.book_store.vo.BookVO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BookDAOImpl implements BookDAO{

    @Override
    public Book getBook(Integer key) {
        Session s = null;
        Book book = null;
        try {
           s = HibernateUtil.getSessionFactory().openSession();
           
           book = (Book) s.get(Book.class, key);
        
        }finally{
             if (s != null) {
                try {
                    s.close();
                } catch (HibernateException e) {
                
                }
            }
        }
        return book;
    }

    @Override
    public void deleteBook(Integer key) {
        Session s = null;
        Transaction  tx = null;
        try {
           s = HibernateUtil.getSessionFactory().openSession();
           tx = s.beginTransaction();
           
           s.createQuery("DELETE FROM book WHERE ISBN= :isbn").setParameter("isbn", key).executeUpdate();
          
           tx.commit();
        }catch(HibernateException e){
            System.out.println(e);
        
        }finally{
             if (s != null) {
                try {
                    s.close();
                } catch (HibernateException e) {
                    System.out.println(e);
                }
            }
        }
        
    }

    @Override
    public void saveCopyBook(Integer Key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
    
}
