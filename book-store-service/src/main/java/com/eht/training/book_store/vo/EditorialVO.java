
package com.eht.training.book_store.vo;

/**
 *
 * @author fabian
 */
public class EditorialVO {
    private int editorial_Id;
    private String editorial;

    public int getEditorial_Id() {
        return editorial_Id;
    }

    public void setEditorial_Id(int editorial_Id) {
        this.editorial_Id = editorial_Id;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
}
