
package com.eht.training.book_store.vo;

/**
 *
 * @author fabian
 */
public class UserRoleVO {
    private UserVO user;
    private RoleVO role;

    public UserVO getUser() {
        return user;
    }

    public void setUser(UserVO user) {
        this.user = user;
    }

    public RoleVO getRole() {
        return role;
    }

    public void setRole(RoleVO role) {
        this.role = role;
    }

    
}
