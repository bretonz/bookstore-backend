package com.eht.training.book_store.dao;

/**
 *
 * @author fabian
 * @param <Entity>
 * @param <Key>
 */
public interface DAO <Entity, Key>{
    Entity getBook(Key key);
    void deleteBook(Key key);
    void saveCopyBook(Key Key);
}
