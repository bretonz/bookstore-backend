package com.eht.training.book_store.dao;

import com.eht.training.book_store.entity.Book;

/**
 *
 * @author fabian
 */
public interface BookDAO extends DAO<Book, Integer>{
    
}

