package com.eht.training.book_store.service;

import com.eht.training.book_store.dao.BookDAO;
import com.eht.training.book_store.entity.Book;
import com.eht.training.book_store.vo.BookVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    
    @Override
    public BookVO getBook(int isbn) {
        if (isbn <= 0) throw new IllegalArgumentException("Numero invalido");
        
        Book book =  this.bookDAO.getBook(isbn);
        
        BookVO bookVO = new BookVO();
        bookVO.setIsbn(book.getIsbn());
        bookVO.setTitle(book.getTitle());
        bookVO.setCategory(null);
        bookVO.setEditorial(null);
         
        return bookVO;
    }
    
     @Override
    public void deleteBook(int isbn) {
         if (isbn <=0 ) throw new IllegalArgumentException("Numero invalido");
         this.bookDAO.deleteBook(isbn);
    }
    
   @Override
    public void saveCopyBook(int isbn) {
         if (isbn <=0 ) throw new IllegalArgumentException("Numero invalido");
         this.bookDAO.saveCopyBook(isbn);
    }

     @Autowired
    private BookDAO bookDAO;
   
    
}
