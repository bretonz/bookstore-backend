
package com.eht.training.book_store.vo;

import java.util.List;

/**
 *
 * @author fabian
 */
public class BookVO {
    private int isbn;
    private String title;
    private CategoryVO category;
    private List<EditorialVO> editorial;

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CategoryVO getCategory() {
        return category;
    }

    public void setCategory(CategoryVO category) {
        this.category = category;
    }

    public List<EditorialVO> getEditorial() {
        return editorial;
    }

    public void setEditorial(List<EditorialVO> editorial) {
        this.editorial = editorial;
    }

    

    
    
    
   
}
